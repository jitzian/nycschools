package mytaxi.challenge.code.com.org.simplecodechallengenycschools.model.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import mytaxi.challenge.code.com.org.simplecodechallengenycschools.model.database.model.ScoresDataClass

@Dao
interface ScoresDao {

    @Insert(onConflict = REPLACE)
    fun insert(scoresDataClass: ScoresDataClass)

    @Query("DELETE FROM scores")
    fun deleteAll()

    @Query("SELECT * FROM scores ORDER BY id ASC")
    fun getAll(): List<ScoresDataClass>

    @Query("SELECT *FROM scores WHERE id = (:id)")
    fun getById(id: Int): ScoresDataClass

}