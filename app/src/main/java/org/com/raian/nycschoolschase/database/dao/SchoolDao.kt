package mytaxi.challenge.code.com.org.simplecodechallengenycschools.model.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import mytaxi.challenge.code.com.org.simplecodechallengenycschools.model.database.model.SchoolDataClass
import mytaxi.challenge.code.com.org.simplecodechallengenycschools.model.database.model.UIDataClass

@Dao
interface SchoolDao {

    @Insert(onConflict = REPLACE)
    fun insert(schoolDataClass: SchoolDataClass)

    @Query("DELETE FROM school")
    fun deleteAll()

    @Query("SELECT * FROM school ORDER BY id ASC")
    fun getAll(): List<SchoolDataClass>

    @Query("SELECT *FROM school WHERE id = (:id)")
    fun getById(id: Int): SchoolDataClass

    @Query("SELECT * FROM school INNER JOIN scores on scores.dbn LIKE school.dbn")
    fun getSchoolsJoin(): List<UIDataClass>?

}