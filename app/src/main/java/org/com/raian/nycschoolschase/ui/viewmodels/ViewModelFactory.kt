package org.com.raian.nycschoolschase.ui.viewmodels

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.persistence.room.Room
import android.content.Context
import org.com.raian.nycschoolschase.constants.GlobalConstants.Companion.dataBaseName
import org.com.raian.nycschoolschase.database.SchoolDataBase

@Suppress("UNCHECKED_CAST")
class ViewModelFactory(private val context: Context): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(SchoolsViewModel::class.java)){
            val db = Room.databaseBuilder(context, SchoolDataBase::class.java, dataBaseName).build()
            return SchoolsViewModel(db) as T
        }
        throw IllegalArgumentException ("View Model Class is not defined")
    }

}