package org.com.raian.nycschoolschase.ui.activities

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import org.com.raian.nycschoolschase.R
import org.com.raian.nycschoolschase.ui.adapters.RVCustomAdapter
import org.com.raian.nycschoolschase.util.NetworkReceiver
import java.util.logging.Logger

class MainActivity : BaseActivity(), NetworkReceiver.NetworkListener {
    private lateinit var mRecyclerViewSchools: RecyclerView
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var rvCustomAdapter: RVCustomAdapter

    init {
        TAG = MainActivity::class.java.simpleName
        logger = Logger.getLogger(TAG)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState).also {
            setContentView(R.layout.activity_main)
            initViews()
            setRecyclerViewItemTouchListener()
        }
    }

    override fun initViews() {
        mRecyclerViewSchools = findViewById(R.id.mRecyclerViewSchools)
        layoutManager = LinearLayoutManager(this)
        mRecyclerViewSchools.layoutManager = layoutManager

        rvCustomAdapter = RVCustomAdapter(this, schoolsViewModel.getListOfSchoolsForUI(), supportFragmentManager)
        mRecyclerViewSchools.adapter = rvCustomAdapter

    }

    private fun prepareObservers() {
        schoolsViewModel.getListOfSchoolsForUI().observe(this, Observer {
            rvCustomAdapter.notifyDataSetChanged()
        })
    }

    private fun setRecyclerViewItemTouchListener() {
        val itemTouchCallback =
            object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    viewHolder1: RecyclerView.ViewHolder
                ): Boolean {
                    return false
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
                    val position = viewHolder.adapterPosition
                    try {
                        mRecyclerViewSchools.adapter?.notifyItemRemoved(position)
                    } catch (indexOutOfBoundException: IndexOutOfBoundsException) {
                        logger.severe("$TAG::setRecyclerViewItemTouchListener::${indexOutOfBoundException.message}")
                    }
                }
            }

        val itemTouchHelper = ItemTouchHelper(itemTouchCallback)
        itemTouchHelper.attachToRecyclerView(mRecyclerViewSchools)
    }

    override fun getNetworkStatus(isConnected: Boolean) {
        try {
            if (!isConnected) {
                logger.severe("$TAG::getNetworkStatus::NO::$isConnected")
                showNoConnectivityMessage()
                prepareObservers()
            } else {
                logger.info("$TAG::getNetworkStatus::YES::$isConnected")
                prepareObservers()
            }
        } catch (rte: RuntimeException) {
            logger.severe("$TAG::getNetworkStatus::${rte.message}")
        }
    }
}
