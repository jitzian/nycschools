package org.com.raian.nycschoolschase.ui.viewmodels

import android.arch.lifecycle.ViewModel
import java.util.logging.Logger

abstract class BaseViewModel: ViewModel(){
    internal lateinit var TAG: String
    internal lateinit var logger: Logger
}