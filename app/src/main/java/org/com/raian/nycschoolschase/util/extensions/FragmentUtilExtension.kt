package org.com.raian.nycschoolschase.util.extensions

import android.annotation.SuppressLint
import android.support.v4.app.Fragment

@SuppressLint("NewApi")
fun Fragment.checkPermissions(){
    val permissions = arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION)
    this.requestPermissions(permissions, 0)
}