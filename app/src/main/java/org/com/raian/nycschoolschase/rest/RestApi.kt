package org.com.raian.nycschoolschase.rest

import org.com.raian.nycschoolschase.rest.model.SchoolsResultRestApi
import org.com.raian.nycschoolschase.rest.model.ScoresResultRestApi
import retrofit2.Call
import retrofit2.http.GET

interface RestApi {
    @GET("s3k6-pzi2.json")
    fun getSchools(): Call<List<SchoolsResultRestApi>>

    @GET("f9bf-2cp4.json")
    fun getScores(): Call<List<ScoresResultRestApi>>
}