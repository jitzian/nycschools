package org.com.raian.nycschoolschase.dependency.injection.components

import dagger.Component
import org.com.raian.nycschoolschase.dependency.injection.modules.NetworkModule
import org.com.raian.nycschoolschase.ui.viewmodels.SchoolsViewModel

@Component(modules = [NetworkModule::class])
interface ComponentInjector {

    fun inject(schoolsViewModel: SchoolsViewModel)

    @Component.Builder
    interface Builder{
        fun build(): ComponentInjector
        fun networkModule(networkModule: NetworkModule): Builder
    }

}