package org.com.raian.nycschoolschase.ui.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import mytaxi.challenge.code.com.org.simplecodechallengenycschools.model.database.model.SchoolDataClass
import mytaxi.challenge.code.com.org.simplecodechallengenycschools.model.database.model.ScoresDataClass
import mytaxi.challenge.code.com.org.simplecodechallengenycschools.model.database.model.UIDataClass
import org.com.raian.nycschoolschase.database.SchoolDataBase
import org.com.raian.nycschoolschase.dependency.injection.components.DaggerComponentInjector
import org.com.raian.nycschoolschase.dependency.injection.modules.NetworkModule
import org.com.raian.nycschoolschase.rest.RestApi
import org.com.raian.nycschoolschase.rest.model.SchoolsResultRestApi
import org.com.raian.nycschoolschase.rest.model.ScoresResultRestApi
import org.com.raian.nycschoolschase.util.safeLet
import retrofit2.Retrofit
import java.util.logging.Logger
import javax.inject.Inject

class SchoolsViewModel(private val dataBase: SchoolDataBase) : BaseViewModel() {
    @Inject
    lateinit var retrofit: Retrofit

    private lateinit var restApi: RestApi

    //This variable is for giving update to UI about the request that is performed
    private val statusOfResponse by lazy {
        MutableLiveData<Boolean>()
    }

    private val listOfSchoolsForUI by lazy {
        MutableLiveData<List<UIDataClass>>()
    }

    private var injector = DaggerComponentInjector
        .builder()
        .networkModule(NetworkModule())
        .build()

    init {
        TAG = SchoolsViewModel::class.java.simpleName
        logger = Logger.getLogger(TAG)
        inject()
        checkLocalData()
    }

    private fun inject() {
        injector.inject(this)
        restApi = retrofit.create(RestApi::class.java)
    }

    private fun checkLocalData() {
        GlobalScope.launch(Dispatchers.IO) {
            val lstRes: List<UIDataClass>? = dataBase.schoolDao().getSchoolsJoin()
            if (lstRes.isNullOrEmpty()) {
                prepareRemoteData()
            } else {
                getStoredDataForUI()
            }
        }
    }

    private fun prepareRemoteData() = GlobalScope.launch(Dispatchers.IO) {
        val deferredSchools = async {
            restApi.getSchools().execute().body()
        }
        val deferredScores = async {
            restApi.getScores().execute().body()
        }

        safeLet(deferredSchools.await(), deferredScores.await()) { schools, scores ->
            storeDataInDb(schools as? List<SchoolsResultRestApi>, scores as? List<ScoresResultRestApi>)
        }
    }

    private fun storeDataInDb(lstOfSchools: List<SchoolsResultRestApi>?, listOfScores: List<ScoresResultRestApi>?) =
        GlobalScope.launch(Dispatchers.IO) {
            withContext(Dispatchers.Default) {

                lstOfSchools?.let { lstSchools ->
                    for (i in lstSchools) {
                        with(i) {
                            val innerSchool = SchoolDataClass(
                                attendanceRate = attendanceRate,
                                bbl = bbl,
                                bin = bin,
                                boro = boro,
                                borough = borough,
                                buildingCode = buildingCode,
                                bus = bus,
                                censusTract = censusTract,
                                city = city,
                                code1 = code1,
                                communityBoard = communityBoard,
                                councilDistrict = councilDistrict,
                                dbn = dbn,
                                directions1 = directions1,
                                ellPrograms = ellPrograms,
                                extracurricularActivities = extracurricularActivities,
                                faxNumber = faxNumber,
                                finalgrades = finalgrades,
                                interest1 = interest1,
                                latitude = latitude,
                                location = location,
                                longitude = longitude,
                                method1 = method1,
                                neighborhood = neighborhood,
                                nta = nta,
                                offerRate1 = offerRate1,
                                overviewParagraph = overviewParagraph,
                                pctStuEnoughVariety = pctStuEnoughVariety,
                                pctStuSafe = pctStuSafe,
                                phoneNumber = phoneNumber,
                                primaryAddressLine1 = primaryAddressLine1,
                                program1 = program1,
                                school10thSeats = school10thSeats,
                                schoolAccessibilityDescription = schoolAccessibilityDescription,
                                schoolEmail = schoolEmail,
                                schoolName = schoolName,
                                schoolSports = schoolSports,
                                subway = subway,
                                website = website,
                                zip = zip,
                                academicopportunities3 = academicopportunities3,
                                addtlInfo1 = addtlInfo1,
                                eligibility1 = eligibility1,
                                languageClasses = languageClasses,
                                transfer = transfer
                            )
                            dataBase.schoolDao().insert(innerSchool)
                        }
                    }
                }

                listOfScores?.let { listScores ->
                    for (i in listScores) {
                        with(i) {
                            val innerScores = ScoresDataClass(
                                dbn = dbn,
                                readingScore = readingScore,
                                mathScore = mathScore,
                                writingScore = writingScore,
                                schoolName = schoolName,
                                numOfSatTestTakers = numOfSatTestTakers
                            )
                            dataBase.scoresDao().insert(innerScores)
                        }
                    }
                }
                listOfSchoolsForUI.postValue(dataBase.schoolDao().getSchoolsJoin())
            }
        }

    private fun getStoredDataForUI() = GlobalScope.launch(Dispatchers.IO) {
        listOfSchoolsForUI.postValue(dataBase.schoolDao().getSchoolsJoin())
    }

    fun getListOfSchoolsForUI(): LiveData<List<UIDataClass>> {
        return listOfSchoolsForUI
    }
}