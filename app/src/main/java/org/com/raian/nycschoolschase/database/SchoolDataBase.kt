package org.com.raian.nycschoolschase.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import mytaxi.challenge.code.com.org.simplecodechallengenycschools.model.database.dao.SchoolDao
import mytaxi.challenge.code.com.org.simplecodechallengenycschools.model.database.dao.ScoresDao
import mytaxi.challenge.code.com.org.simplecodechallengenycschools.model.database.model.SchoolDataClass
import mytaxi.challenge.code.com.org.simplecodechallengenycschools.model.database.model.ScoresDataClass
import org.com.raian.nycschoolschase.constants.GlobalConstants

@Database(
    entities = [SchoolDataClass::class, ScoresDataClass::class],
    exportSchema = false,
    version = GlobalConstants.dataBaseVersion
)

abstract class SchoolDataBase : RoomDatabase() {
    abstract fun schoolDao(): SchoolDao
    abstract fun scoresDao(): ScoresDao
}