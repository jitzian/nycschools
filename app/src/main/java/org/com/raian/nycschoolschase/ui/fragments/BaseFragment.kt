package org.com.raian.nycschoolschase.ui.fragments

import android.support.v4.app.Fragment
import android.view.View
import java.util.logging.Logger

abstract class BaseFragment: Fragment() {

    protected lateinit var logger: Logger
    protected lateinit var rootView: View
    abstract fun initView()

}